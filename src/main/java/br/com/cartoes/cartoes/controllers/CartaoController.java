package br.com.cartoes.cartoes.controllers;

import br.com.cartoes.cartoes.models.Cartao;
import br.com.cartoes.cartoes.models.Cliente;
import br.com.cartoes.cartoes.models.dtos.CartaoEntradaDTO;
import br.com.cartoes.cartoes.models.dtos.CartaoRetornoDTO;
import br.com.cartoes.cartoes.services.CartaoService;
import br.com.cartoes.cartoes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


@RestController
@RequestMapping("/cartao")
public class CartaoController {
    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    public Cartao salvarCartao(@RequestBody CartaoEntradaDTO dto){
        Cartao cartao = cartaoService.salvarCartao(dto.transformarParaObjeto(),dto.getIdCliente());
        return cartao;
    }

    @PatchMapping("/{numero}")
    public Cartao ativarCartao(@PathVariable String numero){
        try{
            Cartao cartao = cartaoService.ativarCartao(numero, true);
            return cartao;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/{numero}")
    public CartaoRetornoDTO buscarCartao(@PathVariable String numero){
        try{
            Cartao cartao = cartaoService.buscarCartaoPorNumero(numero);
            return CartaoRetornoDTO.transformaEmDTO(cartao);
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
