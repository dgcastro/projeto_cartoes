package br.com.cartoes.cartoes.controllers;

import br.com.cartoes.cartoes.models.Pagamento;
import br.com.cartoes.cartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class PagamentoController {
    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping("/pagamento")
    public Pagamento pagar(@RequestBody Pagamento pagamento){
        return pagamentoService.salvarPagamento(pagamento);
    }

    @GetMapping("/pagamentos/{idCartao}")
    public Iterable<Pagamento> consultarPagamentos(@RequestParam long idCartao){
        try{
            return pagamentoService.buscarPagamentos(idCartao);
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
