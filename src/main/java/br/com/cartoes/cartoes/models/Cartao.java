package br.com.cartoes.cartoes.models;

import javax.persistence.*;

@Entity
@Table(name = "cartao")
public class Cartao {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name="cartao_id")
    private long id;

    @Column(name="numero")
    private String numero;

    @ManyToOne
    @JoinColumn(name="cliente_id", nullable = false)
    private long idCliente;

    @Column(name = "ativo")
    private boolean ativo;

    public Cartao() {
    }

    public Cartao(String numero, long idCliente){
        this.numero = numero;
        this.idCliente = idCliente;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(long idCliente) {
        this.idCliente = idCliente;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
