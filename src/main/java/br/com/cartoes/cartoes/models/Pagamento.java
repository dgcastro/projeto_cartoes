package br.com.cartoes.cartoes.models;

import net.bytebuddy.dynamic.loading.InjectionClassLoader;

import javax.persistence.*;

@Entity
public class Pagamento {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "pagamento_id")
    private long id;

    @ManyToOne
    @JoinColumn(name = "cartao_id", nullable = false)
    private long cartaoId;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "valor")
    private double valor;

    public Pagamento() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCartaoId() {
        return cartaoId;
    }

    public void setCartaoId(long cartaoId) {
        this.cartaoId = cartaoId;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
