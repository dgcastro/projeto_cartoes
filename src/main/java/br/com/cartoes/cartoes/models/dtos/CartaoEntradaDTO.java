package br.com.cartoes.cartoes.models.dtos;

import br.com.cartoes.cartoes.models.Cartao;

public class CartaoEntradaDTO {
    private String numero;
    private long idCliente;

    public Cartao transformarParaObjeto(){
        return new Cartao(numero, idCliente);
    }

    public CartaoEntradaDTO() {
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(long idCliente) {
        this.idCliente = idCliente;
    }
}
