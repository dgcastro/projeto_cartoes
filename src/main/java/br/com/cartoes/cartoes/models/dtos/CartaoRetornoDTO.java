package br.com.cartoes.cartoes.models.dtos;


import br.com.cartoes.cartoes.models.Cartao;
import net.bytebuddy.asm.Advice;


public class CartaoRetornoDTO {
    private long id;
    private long idCliente;
    private String numero;

    public CartaoRetornoDTO(long id, long idCliente, String numero) {
        this.id = id;
        this.idCliente = idCliente;
        this.numero = numero;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(long idCliente) {
        this.idCliente = idCliente;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public static CartaoRetornoDTO transformaEmDTO(Cartao cartao){
        return new CartaoRetornoDTO(cartao.getId(), cartao.getIdCliente(), cartao.getNumero());
    }
}
