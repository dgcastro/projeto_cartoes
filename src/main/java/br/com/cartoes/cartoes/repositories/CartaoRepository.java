package br.com.cartoes.cartoes.repositories;

import br.com.cartoes.cartoes.models.Cartao;
import org.springframework.data.repository.CrudRepository;

import javax.swing.text.html.Option;
import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Long> {
    Optional<Cartao> findByNumero(String numero);
}
