package br.com.cartoes.cartoes.repositories;

import br.com.cartoes.cartoes.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository <Cliente, Long>{
}
