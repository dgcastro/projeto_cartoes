package br.com.cartoes.cartoes.repositories;

import br.com.cartoes.cartoes.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {
    Iterable<Pagamento> findByCartaoId(long cartaoId);
}
