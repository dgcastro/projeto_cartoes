package br.com.cartoes.cartoes.services;

import br.com.cartoes.cartoes.models.Cartao;
import br.com.cartoes.cartoes.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {
    @Autowired
    private CartaoRepository cartaoRepository;
    @Autowired
    private ClienteService clienteService;

    public Cartao salvarCartao(Cartao cartao, long idCliente) {
        if(clienteService.verificarClientePorId(idCliente)){
            cartao.setAtivo(false);
            cartao.setIdCliente(idCliente);
            Cartao cartaoObjeto = cartaoRepository.save(cartao);
            return cartaoObjeto;
        }else{
            throw new RuntimeException("O id de cliente informado não está valido");
        }
    }


    public Cartao buscarCartaoPorNumero(String numero) throws RuntimeException{
        Optional<Cartao> cartao= cartaoRepository.findByNumero(numero);
        if(cartao.isPresent()){
            return cartao.get();
        }else {
            throw new RuntimeException("Não existe cartão com o número informado.");
        }
    }

    public Cartao ativarCartao(String numero, boolean ativo) throws RuntimeException{
        try{
            Cartao cartao = buscarCartaoPorNumero(numero);
            cartao.setAtivo(ativo);
            return cartao;
        }catch (RuntimeException exception){
            throw new RuntimeException(exception.getMessage());
        }
    }

    public boolean verificarCartaoPorId(long id){
        if(cartaoRepository.existsById(id)){
            return true;
        }
        else{
            return false;
        }
    }

}
