package br.com.cartoes.cartoes.services;

import br.com.cartoes.cartoes.models.Cliente;
import br.com.cartoes.cartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.nio.file.OpenOption;
import java.util.Optional;

@Service
public class ClienteService {
    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente salvarCliente(Cliente cliente) {
        Cliente clienteObjeto = clienteRepository.save(cliente);
        return clienteObjeto;
    }

    public Cliente buscarClientePorId(long id) {
        Optional<Cliente> optionalCliente = clienteRepository.findById(id);
        if(optionalCliente.isPresent()){
            return optionalCliente.get();
        }else{
            throw new RuntimeException("Não existe cliente com o id informado.");
        }
    }

    public boolean verificarClientePorId(long id){
        if(clienteRepository.existsById(id)){
            return true;
        }
        else{
            return false;
        }
    }
}
