package br.com.cartoes.cartoes.services;

import br.com.cartoes.cartoes.models.Cartao;
import br.com.cartoes.cartoes.models.Pagamento;
import br.com.cartoes.cartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PagamentoService {
    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoService cartaoService;

    public Pagamento salvarPagamento(Pagamento pagamento){
        if(cartaoService.verificarCartaoPorId(pagamento.getCartaoId())){
            Pagamento pagamentoObjeto = pagamentoRepository.save(pagamento);
            return pagamento;
        }else{
            throw new RuntimeException("Cartão não encontrado.");
        }
    }

    public Iterable<Pagamento> buscarPagamentos(long idCartao){
        if(cartaoService.verificarCartaoPorId(idCartao)){
            Iterable<Pagamento> pagamentos = pagamentoRepository.findByCartaoId(idCartao);
            return pagamentos;
        }else {
            throw new RuntimeException("Cartão não encontrado");
        }
    }
}
