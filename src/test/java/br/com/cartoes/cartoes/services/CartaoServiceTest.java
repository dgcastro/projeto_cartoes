package br.com.cartoes.cartoes.services;

import br.com.cartoes.cartoes.models.Cartao;
import br.com.cartoes.cartoes.models.Cliente;
import br.com.cartoes.cartoes.repositories.CartaoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import java.util.Optional;

@SpringBootTest
public class CartaoServiceTest {
    @MockBean
    private CartaoRepository cartaoRepository;
    @MockBean
    private ClienteService clienteService;
    @Autowired
    private CartaoService cartaoService;

    private Cliente cliente;
    private Cartao cartao;

    @BeforeEach
    public void setUp(){
        cartao = new Cartao();
        cartao.setId(1);
        cartao.setNumero("123456789");
        cartao.setIdCliente(1);
        cartao.setAtivo(false);
    }

    @Test
    public void testarSalvarCartao(){
        Mockito.when(cartaoRepository.save(Mockito.any(Cartao.class))).thenReturn(cartao);
        Cartao cartaoObjeto = cartaoService.salvarCartao(cartao, 1);
        Assertions.assertEquals(cartao, cartaoObjeto);
    }

    @Test
    public void testarBuscarCartaoPorNumero(){
        Mockito.when(cartaoRepository.findByNumero(Mockito.anyString())).thenReturn(Optional.of(cartao));
        Cartao cartaoObjeto = cartaoService.buscarCartaoPorNumero("123456789");
        Assertions.assertEquals(cartao, cartaoObjeto);
    }

    @Test
    public void testarBuscarCartaoPorNumeroInexistente(){
        Mockito.when(cartaoRepository.findByNumero(Mockito.anyString())).thenReturn(Optional.ofNullable(null));
        Assertions.assertThrows(RuntimeException.class, () -> {cartaoService.buscarCartaoPorNumero("404");});
    }

    @Test
    public void testarAtivarCartao(){
        Mockito.when(cartaoRepository.findByNumero(Mockito.anyString())).thenReturn(Optional.of(cartao));
        Cartao cartaoObjeto = cartaoService.ativarCartao("123456789", true);
        Assertions.assertEquals(true, cartaoObjeto.isAtivo());
    }

    @Test
    public void testarAtivarCartaoInexistente(){
        Mockito.when(cartaoRepository.findByNumero(Mockito.anyString())).thenReturn(Optional.ofNullable(null));
        Assertions.assertThrows(RuntimeException.class, () -> {cartaoService.ativarCartao("404", false);});
    }
}