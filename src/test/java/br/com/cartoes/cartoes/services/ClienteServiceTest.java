package br.com.cartoes.cartoes.services;

import br.com.cartoes.cartoes.models.Cliente;
import br.com.cartoes.cartoes.repositories.ClienteRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import java.util.Optional;

@SpringBootTest
public class ClienteServiceTest {
    @MockBean
    private ClienteRepository clienteRepository;

    @Autowired
    private ClienteService clienteService;

    Cliente cliente;

    @BeforeEach
    public void setUp(){
        cliente = new Cliente();
        cliente.setId(1);
        cliente.setNome("Bobby");
    }

    @Test
    public void testarSalvarCliente(){
        Mockito.when(clienteRepository.save(Mockito.any(Cliente.class))).thenReturn(cliente);
        Cliente clienteObjeto = clienteService.salvarCliente(cliente);
        Assertions.assertEquals(cliente, clienteObjeto);
    }

    @Test
    public void testarBuscarClientePorId(){
        Mockito.when(clienteRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(cliente));
        Cliente clienteObjeto = clienteService.buscarClientePorId(1);
        Assertions.assertEquals(cliente, clienteObjeto);
    }

    @Test
    public void testarBuscarClientePorIdInexistente(){
        Mockito.when(clienteRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(null));
        Assertions.assertThrows(RuntimeException.class, () -> {clienteService.buscarClientePorId(404);});
    }


}
